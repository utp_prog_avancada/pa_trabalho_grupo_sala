// ==========================================================================
// =                                                                        =
// =                                 LOGIN                                  =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _LOGIN_DAO_H_
#define _LOGIN_DAO_H_

#include <iostream>
#include <vector>
#include <exception>

#include "Login.h"

using namespace std;

#define ARQUIVO_LOGIN "logins.txt"

#define ADMIN_USUARIO "admin"
#define ADMIN_SENHA "123"

/** Acoes persistencia de login */
class LoginDAO {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------

    LoginDAO();

    virtual ~LoginDAO() {}

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------

    /**
     * Consulta todos os logins do banco de dados.
     *
     * @return Lista com todos os logins.
     */
    vector<Login> consultar() const;

    // ----------------------------------------------------------------------
    // - EXCEPTIONS                                                         -
    // ----------------------------------------------------------------------
class ErroArquivoException : public exception {
#ifdef __APPLE__   // isso foi incluido para caso de problema devido a diferença de ambiente , pode copiar daqui
    public:
        virtual const char *what() const _NOEXCEPT {
            return "Erro ao abrir o arquivo: " ARQUIVO_LOGIN;
        }
    };
#elif _WIN32
    public:
        virtual const char *what() const noexcept {
            return "Erro ao abrir o arquivo: " ARQUIVO_LOGIN;
        }
    };
#elif __linux__
    public:
        virtual const char *what() const noexcept {
            return "Erro ao abrir o arquivo: " ARQUIVO_LOGIN;
        }
    };

#endif

private:

};


#endif
