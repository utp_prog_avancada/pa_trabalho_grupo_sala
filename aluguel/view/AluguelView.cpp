// ==========================================================================
// =                                                                        =
// =                                 ALUGUEL                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Felipe Lopes                                                    -
// - Autor: Gabriel Pinto                                                   -
// - Autor: Mateus                                                          -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#include "AluguelView.h"
void AluguelView::inserirInformacoes() {
    cout << "*--------------------------------------------------*" << endl;
    cout << "| INSERIR INFORMACOES DO ALUGUEL                   |" << endl;
    cout << "*--------------------------------------------------*" << endl;

    Aluguel novo_aluguel = capturarInformacoes();
    aluguelControl.EfetuaCadastroAluguel(novo_aluguel);
}

Aluguel AluguelView::capturarInformacoes() {
    string cpfCliente = "";
    string placaVeiculo = "";
    string dataInicio = "";
    string dataEntrega = "";

    cout << "Digite o CPF do cliente: " << endl;
    cin >> cpfCliente;
    cout << "Digite a placa do veiculo: " << endl;
    cin >> placaVeiculo;
    cout << "Digite a data de inicio do aluguel" << endl;
    cin >> dataInicio;
    cout << "Digite a data prevista de entrega do veiculo" << endl;
    cin >> dataEntrega;

    return Aluguel(cpfCliente, dataInicio, dataEntrega, placaVeiculo);
}