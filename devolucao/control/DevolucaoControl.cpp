// ==========================================================================
// =                                                                        =
// =                                 DEVOLUCAO                              =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  17/06/2017                                                      -
// --------------------------------------------------------------------------

#include "DevolucaoControl.h"
#include"../../aluguel/control/AluguelControl.h"



void DevolucaoControl::iniciar() const {
    _view->exibirInicio();
}


void DevolucaoControl::pesquisaAluguel(Devolucao devolucao){

AluguelDAO.buscarAluguel();

vector<Aluguel> lista = buscarAluguel();

// Verifica o aluguel
if (find(lista.begin(), lista.end(), devolucao) != lista.end()) {
   _view->exibirResultado(lista);

    // Exibe a tela de devolucao
    _view->exibirInicio();
}
else {
    _view->exibirMensagemErro();
    _view->exibirInicio();
    }
}


void DevolucaoControl::finalizaAluguel(){
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    cout << (now->tm_year + 1900) << '-'
         << (now->tm_mon + 1) << '-'
         <<  now->tm_mday
         << endl;

   }

PagamentoController efetuaPagamento(){
    DevolucaoView.exibirResultado(vector<Aluguel> lista);
    finalizaAluguel();

}
