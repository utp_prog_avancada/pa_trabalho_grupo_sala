#include <fstream>
class DAO{
  ofstream *bd_automovel;
  ofstream *bd_bicicleta;
  public:
  DAO(){
    bd_automovel = new ofstream("Banco_Dados/bd_automovel.txt",ios::app | ios::in | ios::out );
    bd_bicicleta = new ofstream("Banco_Dados/bd_bicicleta.txt",ios::app | ios::in | ios::out );
  }
  ~DAO(){
    bd_automovel->close();
    bd_bicicleta->close();
  }
  void insert(tipos,Automovel*,Bicicleta*);
  void exclui(vector<Automovel> &l1, vector<Bicicleta> &l2,tipos);
  void altera(vector<Automovel> &l1, vector<Bicicleta> &l2,tipos);
  void imprime(vector<Automovel> &l1, vector<Bicicleta> &l2);
};
