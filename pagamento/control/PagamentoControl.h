// ==========================================================================
// =                                                                        =
// =                               PAGAMENTO                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Matheus Santos                                                  -
// - Data:  18/06/2017                                                      -
// --------------------------------------------------------------------------

#ifndef PA_TRABALHO_2017_PAGAMENTOCONTROL_H
#define PA_TRABALHO_2017_PAGAMENTOCONTROL_H

#include "../view/PagamentoView.h"

#include "../model/Pagamento.h"
#include "../model/PagamentoDAO.h"

#include "../../menu/control/MenuControl.h"

#include "../../devolucao/control/DevolucaoControl.h"

#include <iostream>

using namespace std;

class PagamentoController{
public:
    PagamentoController(Devolucao);
    void salvaPgto();
    bool statusPgto();

private:
    float calculaMulta();
    string formaPgto();
};

#endif //PA_TRABALHO_2017_PAGAMENTOCONTROL_H
