class Bicicleta : public Veiculo{
  public:
    Bicicleta(Veiculo v): Veiculo(v){}

    Bicicleta(string aux):Veiculo(aux){}
    string get_cor(){return cor;}
    string get_tipo(){return tipo;}
    string get_marca(){return marca_modelo;}
    int get_ano(){return anoFabricacao;}
    int get_num(){return numberItem;}



    string set_cor(string a){cor =a;}
    string set_tipo(string a){tipo =a;}
    string set_marca(string a){marca_modelo =a ;}
    int set_ano(int a){anoFabricacao =a;}
    int set_num(int a){numberItem = a;}

    void imprime(){
      cout << cor << "\t\t";
      cout << tipo << "\t\t";
      cout << marca_modelo << "\t\t";
      cout << anoFabricacao << "\t\t";
      cout << numberItem << endl;
    }

};

void imprime(vector<Bicicleta> listaAuto){
  for(int j=0;j<listaAuto.size()-1;++j){
    cout << j << "- ";
    listaAuto[j].imprime();
  }
}
