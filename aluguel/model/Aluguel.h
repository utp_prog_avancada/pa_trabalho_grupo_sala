// ==========================================================================
// =                                                                        =
// =                                 ALUGUEL                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Felipe Lopes                                                    -
// - Autor: Gabriel Pinto                                                   -
// - Autor: Mateus                                                          -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#ifndef PA_TRABALHO_2017_ALUGUEL_H
#define PA_TRABALHO_2017_ALUGUEL_H

#include <iostream>
#include <string>

using namespace std;

class Aluguel {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------
    Aluguel(string cpfCliente, string dtInicio, string dtEntrega, string placaVeiculo) :
                                                                    _placaVeiculo(placaVeiculo),
                                                                    _cpfCliente(cpfCliente),
                                                                    _dataInicio(dtInicio),
                                                                    _dataEntrega(dtEntrega){}

    // ----------------------------------------------------------------------
    // - METODOS                                                          -
    // ----------------------------------------------------------------------
    void setDataInicio(string dataInicio){
        _dataInicio = dataInicio;
    }
    void setDataEntrega(string dataEntrega){
        _dataEntrega = dataEntrega;
    }
    string getDataInicio(){
        return _dataInicio;
    }
    string getDataEntrega(){
        return _dataEntrega;
    }
    void setCpf(string cpf){
        _cpfCliente = cpf;
    }
    string getCpf(){
        return _cpfCliente;
    }
    string getPlaca(){
        return _placaVeiculo;
    }
    void setPlaca(string placa){
        _placaVeiculo = placa;
    }


private:
    // ----------------------------------------------------------------------
    // - ATRIBUTOS                                                          -
    // ----------------------------------------------------------------------
    string _dataInicio;
    string _dataEntrega;
    string _cpfCliente;
    string _placaVeiculo;
};


#endif //PA_TRABALHO_2017_ALUGUEL_H
