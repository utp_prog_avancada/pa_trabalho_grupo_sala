// ==========================================================================
// =                                                                        =
// =                                 DEVOLUCAO                              =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  17/06/2017                                                      -
// --------------------------------------------------------------------------

#include "DevolucaoView.h"
#include<stdio.h>
#include<iostream>


void DevolucaoView::exibirInicio() const {

    cout << "*--------------------------------------------------*" << endl;
    cout << "|DEVOLUCAO                                         |" << endl;
    cout << "*--------------------------------------------------*" << endl;
    cout << "* Digite o cpf:" << endl;

  //  Devolucao devolucao = capturarDados();

  //  _control->pesquisaAluguel(devolucao);
}


/*Devolucao DevolucaoView::capturarDados() const {
    string cpf = "";

    cout << "* cpf.......: ";
    cin >> cpf;

    return Devolucao(cpf);
}
*/

void DevolucaoView::exibirMensagemErro() const {
    cout << endl;
    cout << "Nenhum aluguel!" << endl;
    cout << endl;
}

void DevolucaoView::exibirResultado(vector<Aluguel> lista) const {

    for (int i=0;i<lista.size(); i++){

        string dataInicio = lista[i].getDataInicio();
        string dataEntrega = lista[i].getDataEntrega();
        string cpf = lista[i].getCpf();
        string placa = lista[i].getPlaca();

        cout <<"*|Data Inicio:"+ dataInicio + "|data Entrega:" + dataEntrega + "|CPF:" + cpf + "|"+"|Placa:" + placa + "\n";
    }

}


