#include <stdlib.h>
#include "view/relatorioView.cpp"
#include "DAO/relatorio.cpp"
class controller{
public:
  void controll(std::vector<Automovel> &movel, std::vector<Bicicleta> &bici);
};

void controller::controll(std::vector<Automovel> &movel, std::vector<Bicicleta> &bici){
  relatorioDAO relatorio;
  int aux = relatorioView();
  switch (aux) {
    case 1:
      relatorio.gerarRelatorio(movel,bici);
    break;
    case 2:
      relatorio.excluirRelatorio();
    break;
  }
}
