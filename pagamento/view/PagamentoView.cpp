// ==========================================================================
// =                                                                        =
// =                               PAGAMENTO                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Matheus Santos                                                  -
// - Data:  18/06/2017                                                      -
// --------------------------------------------------------------------------

#include "PagamentoView.h"
#include "../../pagamento/control/PagamentoControl.h"

void PagamentoView::exibeValor() {
    cout << "Valor total a pagar: " << static PagamentoController::calculaTotal() << endl;
}