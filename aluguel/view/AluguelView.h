// ==========================================================================
// =                                                                        =
// =                                 ALUGUEL                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Felipe Lopes                                                    -
// - Autor: Gabriel Pinto                                                   -
// - Autor: Mateus                                                          -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#ifndef PA_TRABALHO_2017_ALUGUELVIEW_H
#define PA_TRABALHO_2017_ALUGUELVIEW_H

#include <iostream>
#include <string>

#include "../model/Aluguel.h"
#include "../control/AluguelControl.h"

using namespace std;

class Aluguel;

class AluguelView {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------

    // ----------------------------------------------------------------------
    // - METODOS                                                          -
    // ----------------------------------------------------------------------
    void inserirInformacoes();
    Aluguel capturarInformacoes();

private:
    // ----------------------------------------------------------------------
    // - ATRIBUTOS                                                          -
    // ----------------------------------------------------------------------
    AluguelControl aluguelControl;
};


#endif //PA_TRABALHO_2017_ALUGUELVIEW_H
