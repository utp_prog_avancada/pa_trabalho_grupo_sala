// ==========================================================================
// =                                                                        =
// =                                 ALUGUEL                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Felipe Lopes                                                    -
// - Autor: Gabriel Pinto                                                   -
// - Autor: Mateus                                                          -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#include "AluguelDAO.h"
/*
 * Grava aluguel realizado no Banco de Dados
 * */
bool AluguelDAO::gravarAluguel(Aluguel novo_aluguel) const{

    string data_inicio = novo_aluguel.getDataInicio();
    string data_entrega = novo_aluguel.getDataEntrega();
    string cpfCliente = novo_aluguel.getCpf();
    string placaVeiculo = novo_aluguel.getPlaca();

    cout << "Gravando no banco de dados" << endl;
    cout << cpfCliente << ";" << data_inicio << ";" << data_entrega << ";" << placaVeiculo << endl;

    ofstream arquivo("aluguel.txt", ios_base::app);

    if(arquivo.is_open()){
        arquivo << cpfCliente << ";" << data_inicio << ";" << data_entrega << ";" << placaVeiculo << endl;
        arquivo.close();
        cout << "Aluguel gravado com sucesso!" << endl;
        return true;
    }else{
        cout << "Erro ao abrir arquivo." << endl;
        return false;
    }
}
/*
 * Busca Aluguel no BD
 * */
vector<Aluguel> AluguelDAO::buscarAluguel() {
    //Criando vetor que sera carrega o aluguel
    vector<Aluguel> vecAluguel;
    ifstream input("aluguel.txt");

    if (input.fail()) {
        cout << "Erro ao tentar abrir o banco de dados aluguel" << endl;
    }

    while (!input.eof()) {
        //CPF Utilizado como chave primaria (?)
        string cpfCliente = "";
        string dataInicio = "";
        string dataEntrega = "";
        string placaVeiculo = "";

        int situacao;

        vecAluguel.push_back(Aluguel(cpfCliente,dataInicio,dataEntrega,placaVeiculo));
    }
    input.close();

    //Retornar vetor de alugueis
    return vecAluguel;
}