// ==========================================================================
// =                                                                        =
// =                                 CLIENTE                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#include "ClienteView.h"

ClienteView::ClienteView(ClienteControl *control) : clienteControl(control){

}

ClienteView::~ClienteView()
{

}
void ClienteView::exibirTelaConsultaCliente() const
{
    int op;
    string rg;
    string cpf;

    cout << "*--------------------------------------------------*" << endl;
    cout << "| CONSULTA DE CLIENTES                             |" << endl;
    cout << "*--------------------------------------------------*" << endl;
    cout << "*Busque por RG(1) | CPF(2) | Sem Filtro(3) *" << endl;
    cout << "*Escolha sua opcao:";
    cin >> op;

    switch (op)
    {
    case 1 :
        cout << "Digite o RG:";
        cin >> rg;
        clienteControl->BuscaClienteRg(rg);
        return clienteControl->VoltarAoMenu();
    case 2 :
        cout << "Digite o CPF:";
        cin >> cpf;
        clienteControl->BuscaClienteCPF(cpf);
        return clienteControl->VoltarAoMenu();
    case 3 :
        clienteControl->BuscaCliente();
        return clienteControl->VoltarAoMenu();
    default:
        return;
        break;
    }
}

void ClienteView::exibirTelaCadastroCliente() const
{

    cout << "*--------------------------------------------------*" << endl;
    cout << "| CADASTRO DE CLIENTES                             |" << endl;
    cout << "*--------------------------------------------------*" << endl;
    cout << "* Digite o nome,rg,cpf e situacao financeira" << endl;

    //Cliente *novo_cliente;
    Cliente novo_cliente = CapturarDadosCliente();

    if(clienteControl->EfetuaCadastroCliente(novo_cliente)){
        this->MenssagemClienteOK();
        clienteControl->VoltarAoMenu();
    }
    else{
        MenssagemClienteErro();
        clienteControl->VoltarAoMenu();
    }
    return;
}
Cliente ClienteView::CapturarDadosCliente() const
{

    string nome = "";
    string rg = "";
    string cpf = "";
    string situacao_financeira = "";

    cout << "* Nome.......: ";
    cin >> nome;

    cout << "* Rg.........: ";
    cin >> rg;

    cout << "* Cpf........: ";
    cin >> cpf;

    cout << "* Situacao financeira(0/1) Boa/Ruim........: ";
    cin >> situacao_financeira;

    return Cliente(nome,rg,cpf,situacao_financeira);

}
void ClienteView::MenssagemClienteErro() const
{
    cout << "Ocorreu algum problema durante a inserção do novo cliente \n \n" << endl;
    return;
}
void ClienteView::MenssagemClienteOK() const
{
    cout << "Cliente inserido no sistema com sucesso \n \n" << endl;
    return;
}

void ClienteView::exibirResultadoDeBusca(vector<Cliente> listaRes)
{
    //for (vector<Cliente>::const_iterator i = listaRes.begin(); i != listaRes.end(); ++i)
     //   cout << i << ' ' << endl;
    cout << "*--------------------------------------------------*" << endl;
    cout << "| RESULTADO DA BUSCA                               |" << endl;
    cout << "*--------------------------------------------------*" << endl;
    int i;
    for (int i=0;i<listaRes.size(); i++){

        string nome = listaRes[i].getClienteNome();
        string rg = listaRes[i].getClienteRg();
        string cpf = listaRes[i].getClienteCPF();
        string situacao = listaRes[i].getClienteSituacaoFinanceira();

        cout <<"*|Nome:"+ nome + "|RG:" + rg + "|CPF:" + cpf + "|"+"|Situacao Financeira:" + situacao + "\n";
    }
}
