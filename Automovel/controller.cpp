typedef enum tipos {automovel=1, bicicleta=2, localizar, Remove,Alterar} tipos;
#include <iostream>
#include <vector>
using namespace std;

#include "model/veiculo.h"
#include "model/automovel.h"
#include "model/bicicleta.h"
#include "view/veiculoview.cpp"
#include "DAO/dao.cpp"

void Localizar(vector<Automovel> listaAutoAutomovel, vector<Bicicleta> listaAutoBicicleta){

  string placa = "abc";
  string tipoCombustivel= "";
  string cor = "";
  string tipo = "";
  string marca = "";
  int ano = -1;
  int num = -1;

  // cout << "placa: ";
  // cin >> placa;
  // cout << "tipo Combustivel: ";
  // cin  >> tipoCombustivel;
  // cout << "cor: ";
  // cin  >> cor;
  // cout << "tipo: ";
  // cin  >> tipo;
  // cout << "marca: ";
  // cin  >> marca;
  // cout << "ano: ";
  // cin  >> ano;
  // cout << "numero: ";
  // cin  >> num;

  cout << "automovel:\n";
  localizarAutomovel(placa,tipoCombustivel,cor,tipo,marca,ano,num, listaAutoAutomovel);
  cout << "\n\nbicicleta:\n";
  localizarBicicleta(cor,tipo,marca,ano,num, listaAutoBicicleta);
}

int main(int argc, char const *argv[]) {
  DAO dao;
  std::vector<Automovel> listaAutoAutomovel;
  std::vector<Bicicleta> listaAutoBicicleta;
  dao.DAO_imprime(listaAutoAutomovel, listaAutoBicicleta);
  Veiculoview Vv;
  tipos t = Vv.mostraDados();


  if(t == automovel){
    Automovel am;
    listaAutoAutomovel.push_back(am);
    dao.DAO_append(automovel, &am,NULL);
  }
  else if(t == bicicleta){
    Bicicleta b;
    listaAutoBicicleta.push_back(b);
    dao.DAO_append(bicicleta,NULL,&b);
  }
  else if(t == Remove){
    int opc;
    cout << "1- automovel\n2-bicicleta\nopc:";
    cin  >> opc;
    if(opc == 1){
      imprime(listaAutoAutomovel);
      DAO_atualiza(listaAutoAutomovel,listaAutoBicicleta,automovel);
    }
    else{
      imprime(listaAutoBicicleta);
      DAO_atualiza(listaAutoAutomovel,listaAutoBicicleta,bicicleta);
    }

  }
  else if(t == localizar){
    cout << endl << "procura\n";
    Localizar(listaAutoAutomovel,listaAutoBicicleta);
  }
  else if(t == Alterar){
    int opc;
    cout << "1- automovel\n2-bicicleta\nopc:";
    cin  >> opc;
    if(opc == 1){
      imprime(listaAutoAutomovel);
      DAO_altera(listaAutoAutomovel,listaAutoBicicleta,automovel);
    }
    else{
      imprime(listaAutoBicicleta);
      DAO_altera(listaAutoAutomovel,listaAutoBicicleta,bicicleta);
    }
  }

  cout<<endl;
  return 0;
}
