// ==========================================================================
// =                                                                        =
// =                                 LOGIN                                  =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------
#include <fstream>

#include "LoginDAO.h"

using namespace std;

vector<Login> LoginDAO::consultar() const {

    // 1. Abre o arquivo
    ifstream input(ARQUIVO_LOGIN);
    if (input.fail()) {
        throw ErroArquivoException();
    }

    // 2. Carrega todos os dados do arquivo
    vector<Login> lista;

    while (!input.eof()) {
        string usuario = "";
        string senha = "";

        input >> usuario >> senha;

        lista.push_back(Login(usuario, senha));
    }

    // 3. Fecha o arquivo
    input.close();

    return lista;
}

LoginDAO::LoginDAO() {
    // Se o arquivo nao existir, cria ele e adiciona o login de administrador
    ifstream file(ARQUIVO_LOGIN);

    if (file.fail()) {

        ofstream newfile(ARQUIVO_LOGIN);
        if (newfile.fail()) {
            throw ErroArquivoException();
        }

        newfile << ADMIN_USUARIO << endl;
        newfile << ADMIN_SENHA << endl;

        newfile.close();
    }


}


