// ==========================================================================
// =                                                                        =
// =                                 CLIENTE                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _CLIENTE_H_
#define _CLIENTE_H_

#include <iostream>

using namespace std;

class Cliente{
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------
    Cliente(string cli_nome ="" , string cli_rg = "", string cli_cpf = "" , string cli_situacao_financeira = "" ) :
    _cli_nome(cli_nome) ,
    _cli_rg(cli_rg),
    _cli_cpf(cli_cpf),
    _cli_situacao_financeira(cli_situacao_financeira)
    {
        _cli_id = ++sequencia;
    }
    virtual ~Cliente() {}

    //ATRIBUTOS
    int sequencia=0;

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------

    void setCliente(string cli_nome) { this->_cli_nome = cli_nome; }
    void setRg(string cli_rg) { this->_cli_rg = cli_rg; }
    void setCPF(string cli_cpf) { this->_cli_cpf = cli_cpf; }
    void setSituacaoFinanceira(int cli_situacao_financeira) { this->_cli_situacao_financeira = cli_situacao_financeira; }

    int getId(){ return this->_cli_id;}
    string getClienteNome() { return this->_cli_nome; }
    string getClienteRg() { return this->_cli_rg; }
    string getClienteCPF() { return this->_cli_cpf; }
    string getClienteSituacaoFinanceira() { return this->_cli_situacao_financeira; }



private:

    string _cli_nome;
    int _cli_id;
    string _cli_cpf;
    string _cli_rg;
    string _cli_situacao_financeira;

};

#endif
