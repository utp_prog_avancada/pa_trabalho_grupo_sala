// ==========================================================================
// =                                                                        =
// =                                 MENU                                   =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------

#include <stdlib.h>

#include "MenuView.h"

void MenuView::exibirTela() const {
    bool saida = false;

    while (!saida) {
        cout << "*--------------------------------------------------*" << endl;
        cout << "| MENU PRINCIPAL                                   |" << endl;
        cout << "*--------------------------------------------------*" << endl;
        cout << "|  1) Consultar Veiculos                           |" << endl;
        cout << "|  2) Consultar Clientes                           |" << endl;
        cout << "|  3) Cadastrar Clientes                           |" << endl;
        cout << "|  4) Devolver um veiculo                          |" << endl;
        cout << "|  5) Alugar veiculo                               |" << endl;
        cout << "|  0) Logout                                       |" << endl;
        cout << "| 99) Sair                                         |" << endl;
        cout << "*--------------------------------------------------*" << endl;
        cout << endl;

        int opt = capturarOpcao();

        saida = tratarOpcao(opt);
    }
}

int MenuView::capturarOpcao() const {
    int opt = 0;
    cout << ">: ";
    cin >> opt;
    return opt;
}

bool MenuView::tratarOpcao(int opt) const {
    switch (opt) {

        case Menu::LOGOUT:
            return doLogout();

        case Menu::CONSULTAR_CLIENTES:
            return doConsultarClientes();

        case Menu::CADASTRAR_CLIENTES:
            return doCadastrarClientes();

        case Menu::DEVOLUCAO_VEICULO:
            return doDevolucaoVeiculo();

        case Menu::ALUGAR_VEICULO:
            return doAlugarVeiculo();

        case Menu::SAIR:
            return doSair();

        default:
            cout << "Opcao invalida!" << endl;
    }
    return false;
}

bool MenuView::doLogout() const {
    return true;
}

bool MenuView::doSair() const {
    exit(1);
    return true;
}

bool MenuView::doConsultarClientes() const {
    ClienteControl control;
    control.IniciarConsultaCliente();
    return true;
}

bool MenuView::doCadastrarClientes() const {
    ClienteControl control;
    control.IniciarCadastroCliente();
    return true;
}
bool MenuView::doDevolucaoVeiculo() const {
    // fazendo nada aqui por enquanto
    return true;
}

/*
 * Iniciando o aluguel buscando o Cliente
 * */
bool MenuView::doAlugarVeiculo() const {
    _aluguel_view->inserirInformacoes();
    return true;
}
