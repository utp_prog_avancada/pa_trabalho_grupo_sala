// ==========================================================================
// =                                                                        =
// =                                 CLIENTE                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#ifndef _CLIENTE_CONTROL_H_
#define _CLIENTE_CONTROL_H_

#include "../model/ClienteDAO.h"
#include "../model/Cliente.h"
#include "../view/ClienteView.h"


#include <stdio.h>

class ClienteControl{
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------
    ClienteControl();
    virtual ~ClienteControl();


    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------
    bool EfetuaCadastroCliente(Cliente &cliente) const;
    void ValidaCPF(string cpf);
    void ValidaRG(string rg);
    void ExibeFichaDoCliente(Cliente);
    void IniciarCadastroCliente();
    void IniciarConsultaCliente();
    void BuscaCliente();
    void BuscaClienteRg(string rg);
    void BuscaClienteCPF(string cpf); // daria para fazer de outra forma, mas vai ficar assim mesmo
    void VoltarAoMenu();


private:
    ClienteDAO registraCliente;
    ClienteView *view;
};

#endif
