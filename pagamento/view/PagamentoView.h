// ==========================================================================
// =                                                                        =
// =                               PAGAMENTO                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Matheus Santos                                                  -
// - Data:  18/06/2017                                                      -
// --------------------------------------------------------------------------

#ifndef PA_TRABALHO_2017_PAGAMENTOVIEW_H
#define PA_TRABALHO_2017_PAGAMENTOVIEW_H

#include <iostream>

class PagamentoView{
public:
    void exibeValor();

};

#endif //PA_TRABALHO_2017_PAGAMENTOVIEW_H
