// ==========================================================================
// =                                                                        =
// =                                 CLIENTE                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _CLIENTE_VIEW_H_
#define _CLIENTE_VIEW_H_

#include "../control/ClienteControl.h"
#include "../model/Cliente.h"
#include "../../menu/control/MenuControl.h"

class ClienteControl;

class ClienteView{
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------
   ClienteView(ClienteControl *control);
   ~ClienteView();



   // ----------------------------------------------------------------------
   // - METODOS                                                            -
   // ----------------------------------------------------------------------

   /**
    * Exibe e captura os dados do login.
    */
   void exibirTelaCadastroCliente() const;
   void exibirTelaConsultaCliente() const;
   void exibirResultadoDeBusca(vector<Cliente> listaRes);

   Cliente CapturarDadosCliente() const;
   void MenssagemClienteOK() const;
   void MenssagemClienteErro() const;

private:
   ClienteControl *clienteControl;
};

#endif
