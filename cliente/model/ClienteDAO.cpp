// ==========================================================================
// =                                                                        =
// =                                 CLIENTE                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#include "ClienteDAO.h"


ClienteDAO::ClienteDAO()
{
    ifstream file(ARQUIVO_CLIENTES);

    if (file.fail()) {

        ofstream newfile(ARQUIVO_CLIENTES);
        if (newfile.fail()) {
            throw ErroArquivoException();
        }
        newfile.close();
    }
}

vector<Cliente> ClienteDAO::ConsultarClientes() const
{
    string line;
    vector<Cliente> lista;
    ifstream input(ARQUIVO_CLIENTES);
    if (input.fail()) {
        throw ErroArquivoException();
    }

    while (!input.eof()) {
        string id;
        string nome = "";
        string rg = "";
        string cpf = "";
        string situacao;

        //getline (ARQUIVO_CLIENTES,line);

        //input >> id >> nome >> rg >> cpf >> situacao;
        input >> nome >> rg >> cpf >> situacao;

        lista.push_back(Cliente(nome,rg,cpf,situacao));
    }
    input.close();

    return lista;
}
bool ClienteDAO::InserindoNovoCliente(Cliente cliente_novo) const
{
    string id = to_string(cliente_novo.getId());
    string nome = cliente_novo.getClienteNome();
    string rg = cliente_novo.getClienteRg();
    string cpf = cliente_novo.getClienteCPF();
    string situacao = cliente_novo.getClienteSituacaoFinanceira();

    //ifstream file(ARQUIVO_CLIENTES);

    //if (file.fail()) {
        ofstream newfile(ARQUIVO_CLIENTES);
        if (newfile.fail()) {
            throw ErroArquivoException();
        }
        newfile << nome << endl;
        newfile << rg << endl;
        newfile << cpf << endl;
        newfile << situacao << endl;

        newfile.close();
        return true;
    //}
    //return false;

}
