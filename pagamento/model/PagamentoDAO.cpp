// ==========================================================================
// =                                                                        =
// =                               PAGAMENTO                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Matheus Santos                                                  -
// - Data:  18/06/2017                                                      -
// --------------------------------------------------------------------------

#include "PagamentoDAO.h"

PagamentoDAO::PagamentoDAO() {
    ifstream file(ARQ_PGTO);

    if (file.fail()) {

        ofstream pagamento(ARQ_PGTO);
        if (pagamento.fail()) {
            exit(-1);
        }
        pagamento.close();
    }
}

void PagamentoDAO::gravaPgto() {
    ofstream pagamento;

    pagamento.open("../../registros/pagamento.txt", ARQ_PGTO);


}