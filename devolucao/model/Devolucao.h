// ==========================================================================
// =                                                                        =
// =                                 DEVOLUCAO                              =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  17/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _DEVOLUCAO_H_
#define _DEVOLUCAO_H_
#include <string>

#include"../../cliente/model/Cliente.h"
#include "../../Automovel/model/veiculo.h"


using namespace std;

class Devolucao {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------

    Devolucao( string cpf = "") : _cpf(cpf){}

    virtual ~Devolucao();

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------
    void setCpf(string cpf) { _cpf = cpf; }

    string getCpf() { return _cpf; }


private:
    // ----------------------------------------------------------------------
    // - ATRIBUTOS                                                          -
    // ----------------------------------------------------------------------
    string _cpf;

};


#endif
