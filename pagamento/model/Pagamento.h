// ==========================================================================
// =                                                                        =
// =                               PAGAMENTO                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Matheus Santos                                                  -
// - Data:  18/06/2017                                                      -
// --------------------------------------------------------------------------

#ifndef PA_TRABALHO_2017_PAGAMENTO_H
#define PA_TRABALHO_2017_PAGAMENTO_H

#include <iostream>

using namespace std;

class Pagamento{
public:
    Pagamento();
    ~Pagamento();


    float getValorAluguel();
    void setValorPago();
    void setDtDevolucao();

private:
    string _dtFimAluguel;
    string _dtDevolucao;
    float _valorPago;
};

#endif //PA_TRABALHO_2017_PAGAMENTO_H

