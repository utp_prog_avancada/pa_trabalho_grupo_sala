// ==========================================================================
// =                                                                        =
// =                                 CLIENTE                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#include "ClienteControl.h"

ClienteControl::ClienteControl() : view(new ClienteView(this))
{

}

ClienteControl::~ClienteControl()
{
    delete view;
}

void ClienteControl::IniciarCadastroCliente()
{
    view->exibirTelaCadastroCliente();
}
void ClienteControl::IniciarConsultaCliente()
{
    view->exibirTelaConsultaCliente();
}

bool ClienteControl::EfetuaCadastroCliente(Cliente &cliente) const
{
    if(registraCliente.InserindoNovoCliente(cliente)){
        return true;
    }
    else{
        return false;
    }

}
void ClienteControl::ValidaCPF(string cpf) // 11 numeros
{
    if(cpf.size() > 11){
        cout << "numero de digitos invalidos para o cpf"<<endl;
    }

}
void ClienteControl::ValidaRG(string rg)
{
    if(rg.size() > 9){
        cout << "numero de digitos invalidos para o rg"<<endl;
    }
}

void ClienteControl::VoltarAoMenu()
{
    MenuControl control;
    control.iniciar();
}
void ClienteControl::BuscaClienteCPF(string cpf)
{
    vector<Cliente> lista;
    //implementar busca solicitando coisas da DAO
    view->exibirResultadoDeBusca(lista);
    return;
}
void ClienteControl::BuscaClienteRg(string rg)
{
    vector<Cliente> lista;
    //implementar busca solicitando coisas da DAO
    view->exibirResultadoDeBusca(lista);
    return;
}

void ClienteControl::BuscaCliente()
{
    vector<Cliente> lista;
    lista = registraCliente.ConsultarClientes();
    //implementar busca solicitando coisas da DAO
    view->exibirResultadoDeBusca(lista);
    return;
}
