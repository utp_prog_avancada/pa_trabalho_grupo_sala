// ==========================================================================
// =                                                                        =
// =                                 LOGIN                                  =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------

#include "LoginView.h"

void LoginView::exibirTela() const {

    cout << "*--------------------------------------------------*" << endl;
    cout << "| LOGIN                                            |" << endl;
    cout << "*--------------------------------------------------*" << endl;
    cout << "* Digite o usuario e senha:" << endl;

    Login login = capturarLogin();

    _control->autenticarLogin(login);
}

Login LoginView::capturarLogin() const {
    string usuario = "";
    string senha = "";

    cout << "* usuario.......: ";
    cin >> usuario;

    cout << "* senha.........: ";
    cin >> senha;

    return Login(usuario, senha);
}

void LoginView::exibirMensagemErro() const {
    cout << endl;
    cout << "!!! Login invalido !!!" << endl;
    cout << endl;
}

void LoginView::exibirMensagemSucesso() const {
    cout << endl;
    cout << "Usuário autenticado com sucesso" << endl;
    cout << endl;
}

