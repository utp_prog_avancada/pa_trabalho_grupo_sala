// ==========================================================================
// =                                                                        =
// =                               PAGAMENTO                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Matheus Santos                                                  -
// - Data:  18/06/2017                                                      -
// --------------------------------------------------------------------------

#ifndef PA_TRABALHO_2017_PAGAMENTODAO_H
#define PA_TRABALHO_2017_PAGAMENTODAO_H

#include <iostream>
#include <fstream>

#define ARQ_PGTO "../../registros/pagamento.txt"
using namespace std;

class PagamentoDAO{
public:
    PagamentoDAO();
    ~PagamentoDAO();


    void setStatus();
private:
    void gravaPgto();
};

#endif //PA_TRABALHO_2017_PAGAMENTODAO_H
