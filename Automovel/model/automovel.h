class Automovel : public Veiculo{
public:
    string tipoCombustivel;
    string placa;
    Automovel(string tipo, string placa, Veiculo v): tipoCombustivel(tipo), placa(placa), Veiculo(v){}

    Automovel(string aux):Veiculo(aux){}

    string get_cor(){return cor;}
    string get_tipo(){return tipo;}
    string get_marca(){return marca_modelo;}

    int get_ano(){return anoFabricacao;}
    int get_num(){return numberItem;}

    void set_cor(string c){cor=c;}
    void set_tipo(string t){tipo=t;}
    void set_marca(string m){marca_modelo=m;}


    void set_ano(int a){anoFabricacao = a;}
    void set_num(int a){ numberItem =a;}


    void imprime(){
      cout << tipoCombustivel<<  "\t\t";
      cout << placa<< "\t\t";
      cout << get_cor() << "\t\t";
      cout << get_tipo()<< "\t\t";
      cout << get_marca()<< "\t\t";
      cout << get_ano()<< "\t\t";
      cout << get_num()<< endl;
    }

};

void imprime(vector<Automovel> listaAuto){
  for(int j=0;j<listaAuto.size()-1;++j){
    cout << j << "- ";
    listaAuto[j].imprime();
  }
}
