// ==========================================================================
// =                                                                        =
// =                                 LOGIN                                  =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _LOGIN_VIEW_H_
#define _LOGIN_VIEW_H_

#include <iostream>
#include <vector>

#include "../control/LoginControl.h"
#include "../model/Login.h"

// Forward Declaration
// - Resolve o problema de declaracao circular
// - Por causa disso, a classe nao pode ter metodos inline
class LoginControl;

using namespace std;

/** Acoes de interacao com o usuario para exibir a tela de login */
class LoginView {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------

    LoginView(LoginControl *control) : _control(control) {}

    virtual ~LoginView() {}

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------

    /**
     * Exibe e captura os dados do login.
     */
    void exibirTela() const;

    /**
     * Exibe mensagem de erro ao autenticar o login.
     */
    void exibirMensagemErro() const;

    /**
     * Exibe mensagem de sucesso ao autenticar o login.
     */
    void exibirMensagemSucesso() const;

private:
    // METODOS PRIVADOS

    /**
     * Captura os dados do login da tela
     *
     * @return login preenchido
     */
    Login capturarLogin() const;

    // ----------------------------------------------------------------------
    // - ATRIBUTOS                                                          -
    // ----------------------------------------------------------------------
    LoginControl *_control;

};


#endif
