// ==========================================================================
// =                                                                        =
// =                                 LOGIN                                  =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _LOGIN_H_
#define _LOGIN_H_

#include <string>

using namespace std;

/** Representa os dados do login */
class Login {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------

    Login(string usuario = "", string senha = "") : _usuario(usuario), _senha(senha) {}

    virtual ~Login() {}

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------

    void setUsuario(string usuario) { _usuario = usuario; }

    void setSenha(string senha) { _senha = senha; }

    string getUsuario() { return _usuario; }

    string getSenha() { return _senha; }

    bool operator==(const Login &login) {
        return _usuario == login._usuario && _senha == login._senha;
    }

private:
    // ----------------------------------------------------------------------
    // - ATRIBUTOS                                                          -
    // ----------------------------------------------------------------------
    string _usuario;
    string _senha;

};


#endif
