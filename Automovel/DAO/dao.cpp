#include "dao.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
using namespace std;
void DAO::insert(tipos t,Automovel *v,Bicicleta *b){
  if(t == automovel){
    *bd_automovel << v->tipoCombustivel<< " " ;
    *bd_automovel << v->placa<< " " ;
    *bd_automovel << v->get_cor()<< " " ;
    *bd_automovel << v->get_tipo()<< " " ;
    *bd_automovel << v->get_marca()<< " " ;
    *bd_automovel << v->get_ano()<< " " ;
    *bd_automovel << v->get_num() << endl;
  }
  else{
    *bd_bicicleta << b->get_cor() << " " ;
    *bd_bicicleta << b->get_tipo()<< " " ;
    *bd_bicicleta << b->get_marca()<< " " ;
    *bd_bicicleta << b->get_ano()<< " " ;
    *bd_bicicleta << b->get_num() << endl;

  }
}
// exclui
void DAO::exclui(vector<Automovel> &l1, vector<Bicicleta> &l2,tipos t){
  FILE* fauto = fopen("Banco_Dados/bd_automovel.txt", "w");
  FILE* fbici = fopen("Banco_Dados/bd_bicicleta.txt", "w");
  fclose(fauto);
  fclose(fbici);
  DAO dao;
  Veiculoview vv;
  int opc = vv.exclui_altera_bd();

  if(t == automovel){
    l1.erase(l1.begin()+opc);
  }
  else{
    l2.erase(l2.begin()+opc);
  }

  cout << l1.size()-1 << endl;

  for(int i=0;i<l1.size()-1;++i){
    dao.insert(automovel,&l1[i],NULL);
  }

  for(int i=0;i<l2.size()-1;++i){
    dao.insert(bicicleta,NULL,&l2[i]);
  }

}
// altera
void DAO::altera(vector<Automovel> &l1, vector<Bicicleta> &l2, tipos t){
  FILE* fauto = fopen("Banco_Dados/bd_automovel.txt", "w");
  FILE* fbici = fopen("Banco_Dados/bd_bicicleta.txt", "w");
  fclose(fauto);
  fclose(fbici);
  DAO dao;
  Veiculoview vv;
  int opc = vv.exclui_altera_bd();

  if(t == automovel){
    l1[opc] = vv.criaAutomovel();
  }
  else{
    l2[opc] = vv.criaBicicleta();
  }

  for(int i=0;i<l1.size()-1;++i){
    dao.insert(automovel,&l1[i],NULL);
  }

  for(int i=0;i<l2.size()-1;++i){
    dao.insert(bicicleta,NULL,&l2[i]);
  }

}



vector<Automovel> DAO_imprime_carro(){
  string am = "s",b[100] = "s";
  int intaux,i=0;
  std::vector<Automovel> listaAuto;
  Automovel aux("aux");
  ifstream bd("Banco_Dados/bd_automovel.txt");;
  while (!bd.eof()){
    bd >> aux.tipoCombustivel;
    bd >> aux.placa;
    bd >> am;
    aux.set_cor(am) ;
    bd >> am;
    aux.set_tipo(am);
    bd >> am;
    aux.set_marca(am);
    bd >> intaux;
    aux.set_ano(intaux);
    bd >> intaux;
    aux.set_num(intaux);
    listaAuto.push_back(aux);
  }
  bd.close();
  return listaAuto;
}

vector<Bicicleta>  DAO_imprime_bicicleta(){
  string am = "s",b[100] = "s";
  int intaux,i=0;
  std::vector<Bicicleta> listaAuto;
  Bicicleta aux("aux");
  ifstream bd("Banco_Dados/bd_bicicleta.txt");;
  while (!bd.eof()){
    bd >> am;
    aux.set_cor(am);
    bd >> am;
    aux.set_tipo(am);
    bd >> am;
    aux.set_marca(am);
    bd >> intaux;
    aux.set_ano(intaux);
    bd >> intaux;
    aux.set_num(intaux);
    listaAuto.push_back(aux);
  }

  bd.close();
  return listaAuto;
}

void DAO::imprime(std::vector<Automovel> &l1, std::vector<Bicicleta> &l2){
  l1 = DAO_imprime_carro();
  l2 = DAO_imprime_bicicleta();
}
