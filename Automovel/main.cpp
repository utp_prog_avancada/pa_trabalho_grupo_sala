typedef enum tipos {automovel=1, bicicleta=2, criar,  localizar, Remove,Alterar, relatorios} tipos;
#include <iostream>
#include <vector>
using namespace std;

#include "model/veiculo.h"
#include "model/automovel.h"
#include "model/bicicleta.h"

#include "view/localiza_veiculo.h"

#include "view/veiculoview.cpp"
#include "DAO/dao.cpp"


#include "relatorio/controller.cpp"
class controle{
  public:
  void control();
};

void controle::control() {
  DAO dao;
  std::vector<Automovel> listaAutoAutomovel;
  std::vector<Bicicleta> listaAutoBicicleta;
  dao.imprime(listaAutoAutomovel, listaAutoBicicleta);
  Veiculoview Vv;
  tipos aux, t = Vv.mostraDados(aux);


  if(aux == criar){
    if(t == automovel){
      Automovel am = Vv.criaAutomovel();
      listaAutoAutomovel.push_back(am);
      dao.insert(automovel, &am,NULL);
      sucesso();
    }
    else if(t == bicicleta){
        Bicicleta b = Vv.criaBicicleta();
        listaAutoBicicleta.push_back(b);
        dao.insert(bicicleta,NULL,&b);
        sucesso();
    }
  }
  else if(aux == Remove){
    if(t == automovel){
      imprime(listaAutoAutomovel);
      dao.exclui(listaAutoAutomovel,listaAutoBicicleta,automovel);
      sucesso();
    }
    else if(t == bicicleta){
      imprime(listaAutoBicicleta);
      dao.exclui(listaAutoAutomovel,listaAutoBicicleta,bicicleta);
      sucesso();
    }
  }
  else if(aux == Alterar){
    if(t == automovel){
      imprime(listaAutoAutomovel);
      dao.altera(listaAutoAutomovel,listaAutoBicicleta,automovel);
      sucesso();
    }
    else if(t == bicicleta){
      imprime(listaAutoBicicleta);
      dao.altera(listaAutoAutomovel,listaAutoBicicleta,bicicleta);
      sucesso();
    }
  }
  else if(aux == localizar){
    Vv.Localizar(listaAutoAutomovel,listaAutoBicicleta);
    sucesso();
  }
  else if(aux == relatorios){
    controller c;
    c.controll(listaAutoAutomovel,listaAutoBicicleta);
    sucesso();
  }

  cout<<endl;
  return;
}



int main(){
  controle c;
  c.control();
}
