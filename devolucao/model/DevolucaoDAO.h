// ==========================================================================
// =                                                                        =
// =                                 DEVOLUCAO                              =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  17/06/2017                                                      -
// --------------------------------------------------------------------------
#include <vector>
#include <iostream>
#include <exception>

#define ARQUIVO_CLIENTES "../../registros/clientes.txt"

#ifndef _DEVOLUCAO_DAO_H_
#define _DEVOLUCAO_DAO_H_
#include "Devolucao.h"

using namespace std;

class DevolucaoDAO{
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------
    DevolucaoDAO();
    ~DevolucaoDAO();

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------


    // ----------------------------------------------------------------------
    // - EXCEPTIONS                                                         -
    // ----------------------------------------------------------------------
    class ErroArquivoException : public exception {
#ifdef __APPLE__   // isso foi incluido para caso de problema devido a diferença de ambiente , pode copiar daqui
    public:
        virtual const char *what() const _NOEXCEPT {
            return "Erro ao abrir o arquivo: " ARQUIVO_CLIENTES;
        }
    };
#elif _WIN32
    public:
        virtual const char *what() const noexcept {
            return "Erro ao abrir o arquivo: " ARQUIVO_CLIENTES;
        }
    };
#elif __linux__
    public:
        virtual const char *what() const noexcept {
            return "Erro ao abrir o arquivo: " ARQUIVO_CLIENTES;
        }
    };

#endif

private:

};


#endif
