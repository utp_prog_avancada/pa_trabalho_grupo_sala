// ==========================================================================
// =                                                                        =
// =                                 ALUGUEL                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Felipe Lopes                                                    -
// - Autor: Gabriel Pinto                                                   -
// - Autor: Mateus                                                          -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------


#include "AluguelControl.h"

/*
 * Mandar para o model gravar no arquivo
 * */
void AluguelControl::EfetuaCadastroAluguel(Aluguel &aluguel) const {
    registraAluguel.gravarAluguel(aluguel);
}
