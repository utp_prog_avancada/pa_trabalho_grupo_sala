// ==========================================================================
// =                                                                        =
// =                                 LOGIN                                  =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------

#include <algorithm>

#include "LoginControl.h"

using namespace std;

void LoginControl::iniciar() const {
    _view->exibirTela();
}

void LoginControl::autenticarLogin(Login login) const {

    // Consulta todos os logins
    LoginDAO dao;
    vector<Login> lista = dao.consultar();

    // Verifica se o login esta na lista de logins
    if (find(lista.begin(), lista.end(), login) != lista.end()) {
        _view->exibirMensagemSucesso();

        // Redireciona para tela de menu
        MenuControl control;
        control.iniciar();

        // Exibe a tela de login qndo acabar o menu
        _view->exibirTela();
    }
    else {
        _view->exibirMensagemErro();
        _view->exibirTela();
    }
}

LoginControl::LoginControl() : _view(new LoginView(this)) {

}

LoginControl::~LoginControl() {
    delete _view;
}
