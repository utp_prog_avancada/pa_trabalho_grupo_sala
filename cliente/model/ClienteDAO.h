// ==========================================================================
// =                                                                        =
// =                                 CLIENTE                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#ifndef _CLIENTE_DAO_H_
#define _CLIENTE_DAO_H_

#include "Cliente.h"

//#define ARQUIVO_CLIENTES "../../registros/clientes.txt"
#define ARQUIVO_CLIENTES "../../clientes.txt"


#include <iostream>
#include <vector>
#include <exception>
#include <fstream>

using namespace std;

class ClienteDAO{
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------
    ClienteDAO();
    virtual ~ClienteDAO(){}

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------
    vector<Cliente> ConsultarClientes() const;
    bool InserindoNovoCliente(Cliente cliente_novo) const;




class ErroArquivoException : public exception {
#ifdef __APPLE__
    public:
        virtual const char *what() const _NOEXCEPT {
            return "Erro ao abrir o arquivo: " ARQUIVO_CLINTES;
        }
    };
#elif _WIN32
    public:
        virtual const char *what() const noexcept {
            return "Erro ao abrir o arquivo: " ARQUIVO_CLINTES;
        }
    };
#elif __linux__
    public:
        virtual const char *what() const noexcept {
            return "Erro ao abrir o arquivo: " ARQUIVO_CLIENTES;
        }
    };

#endif
    private:
        vector<Cliente> lista_clientes;
};


#endif
