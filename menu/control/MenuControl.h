// ==========================================================================
// =                                                                        =
// =                                 MENU                                   =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _MENU_CONTROL_H_
#define _MENU_CONTROL_H_

#include "../view/MenuView.h"

// Forward Declaration
// - Resolve o problema de declaracao circular
// - Por causa disso, a classe nao pode ter metodos inline
class MenuView;

/** Controle do menu principal da aplicacao */
class MenuControl {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------

    MenuControl();
    virtual ~MenuControl() {}

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------

    /**
     * Inicia o controlador de menu
     */
    virtual void iniciar() const;

private:
    // ----------------------------------------------------------------------
    // - ATRIBUTOS                                                          -
    // ----------------------------------------------------------------------
    MenuView *_view;

};


#endif
