// ==========================================================================
// =                                                                        =
// =                                 ALUGUEL                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Felipe Lopes                                                    -
// - Autor: Gabriel Pinto                                                   -
// - Autor: Mateus                                                          -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------


#ifndef PA_TRABALHO_2017_ALUGUELCONTROL_H
#define PA_TRABALHO_2017_ALUGUELCONTROL_H

#include <iostream>
#include <string>
#include "../../cliente/view/ClienteView.h"
#include "../../cliente/model/Cliente.h"

#include "../model/Aluguel.h"
#include "../model/AluguelDAO.h"

class AluguelControl {
public:
    AluguelControl();
    virtual ~AluguelControl();

    //Metodo para chamar o busca cliente do Alan
    void buscarCliente();

    //Metodo para chamar o busca veiculo da Tati
    void buscarVeiculo();

    //Chama metodo para gravar no txt
    void EfetuaCadastroAluguel(Aluguel &aluguel) const;

private:
    AluguelDAO registraAluguel;
};


#endif //PA_TRABALHO_2017_ALUGUELCONTROL_H
