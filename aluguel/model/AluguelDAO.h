// ==========================================================================
// =                                                                        =
// =                                 ALUGUEL                                =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Felipe Lopes                                                    -
// - Autor: Gabriel Pinto                                                   -
// - Data:  12/06/2017                                                      -
// --------------------------------------------------------------------------

#ifndef PA_TRABALHO_2017_ALUGUELDAO_H
#define PA_TRABALHO_2017_ALUGUELDAO_H

#include <iostream>
#include <vector>
#include <exception>
#include <fstream>
#include "../model/Aluguel.h"

#define ARQUIVO_ALUGUEL "../../registros/aluguel.txt"

using namespace std;

class AluguelDAO {
public:
    AluguelDAO();
    virtual ~AluguelDAO();

    bool gravarAluguel(Aluguel novo_aluguel)const;
    vector<Aluguel> buscarAluguel();

};


#endif //PA_TRABALHO_2017_ALUGUELDAO_H
