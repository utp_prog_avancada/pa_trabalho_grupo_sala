// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  17/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _DEVOLUCAO_CONTROL_H_
#define _DEVOLUCAO_CONTROL_H_

#include "../model/Devolucao.h"
#include "../view/DevolucaoView.h"
#include "../../aluguel/model/AluguelDAO.h"
#include "../../menu/control/MenuControl.h"
#include "../../pagamento/control/PagamentoControl.h"

class DevolucaoView;
class DevolucaoControl{
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------
    DevolucaoControl();
    ~DevolucaoControl();
    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------
    void iniciar() const;
    void pesquisaAluguel(Devolucao devolucao) const;
    void finalizaAluguel();
    //PagamentoController efetuaPagamento(AluguelDAO aluguel);

private:
   DevolucaoView *_view;
};
#endif
