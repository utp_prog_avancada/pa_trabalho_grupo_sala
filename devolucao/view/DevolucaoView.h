// ==========================================================================
// =                                                                        =
// =                                 DEVOLUCAO                              =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Alan Azevedo Bancks                                             -
// - Autor: Bianca Lara Gomes                                               -
// - Data:  17/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _DEVOLUCAO_VIEW_H_
#define _DEVOLUCAO_VIEW_H_

#include <iostream>
#include <vector>
#include "../../aluguel/model/Aluguel.h"

#include "../control/DevolucaoControl.h"
#include "../model/Devolucao.h"

class DevolucaoControl;

class DevolucaoView {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------

    DevolucaoView();

    virtual ~DevolucaoView();

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------
    void exibirTela() const;
    void exibirMensagemErro() const;
    void exibirResultado(vector<Aluguel> lista) const;
    void exibirInicio() const;


private:
 //   DevolucaoView capturarDados() const;

    // ----------------------------------------------------------------------
    // - ATRIBUTOS                                                          -
    // ----------------------------------------------------------------------
    DevolucaoControl * _control;
};


#endif
