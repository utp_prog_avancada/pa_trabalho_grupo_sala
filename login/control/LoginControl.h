// ==========================================================================
// =                                                                        =
// =                                 LOGIN                                  =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _LOGIN_CONTROL_H_
#define _LOGIN_CONTROL_H_

#include "../view/LoginView.h"

#include "../model/Login.h"
#include "../model/LoginDAO.h"

#include "../../menu/control/MenuControl.h"


// Forward Declaration
// - Resolve o problema de declaracao circular
// - Por causa disso, a classe nao pode ter metodos inline
class LoginView;

/** Controle das acoes de login */
class LoginControl {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------

    LoginControl();

    virtual ~LoginControl();

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------

    /**
     * Inicia o controlador de login
     */
    void iniciar() const;

    /**
     * Realiza a autenticacao do login
     *
     * @param login login a ser autenticado
     */
    void autenticarLogin(Login login) const;

private:
    // ----------------------------------------------------------------------
    // - ATRIBUTOS                                                          -
    // ----------------------------------------------------------------------
    LoginView *_view;

};


#endif
