// ==========================================================================
// =                                                                        =
// =                                 MENU                                   =
// =                                                                        =
// ==========================================================================
// --------------------------------------------------------------------------
// - Autor: Chaua Queirolo                                                  -
// - Data:  10/06/2017                                                      -
// --------------------------------------------------------------------------
#ifndef _MENU_VIEW_H_
#define _MENU_VIEW_H_

#include <iostream>

#include "../control/MenuControl.h"
#include "../../cliente/control/ClienteControl.h"
#include "../../cliente/view/ClienteView.h"

#include "../../aluguel/control/AluguelControl.h"
#include "../../aluguel/view/AluguelView.h"

using namespace std;

// Forward Declaration
// - Resolve o problema de declaracao circular
// - Por causa disso, a classe nao pode ter metodos inline
class MenuControl;



enum Menu {
    CONSULTAR_VEICULOS = 1,
    CONSULTAR_CLIENTES = 2,
    CADASTRAR_CLIENTES = 3,
    DEVOLUCAO_VEICULO  = 4,
    ALUGAR_VEICULO  = 5,
    LOGOUT = 0,
    SAIR = 99
};

/**
 * Acoes de interacao com o usuario para exibir a tela de menu
 *
 * TODO: TODAS NOVAS FUNCIONALIDADES DEVEM TER UMA ENTRADA AQUI!!!
 */
class MenuView {
public:
    // ----------------------------------------------------------------------
    // - CONSTRUTORES / DESTRUTOR                                           -
    // ----------------------------------------------------------------------

    MenuView(MenuControl *control) : _control(control) {}

    virtual ~MenuView() {}

    // ----------------------------------------------------------------------
    // - METODOS                                                            -
    // ----------------------------------------------------------------------

    /**
     * Exibe e tela de menu.
     */
    virtual void exibirTela() const;

private:
    // METODOS PRIVADOS
    int capturarOpcao() const;

    bool tratarOpcao(int opt) const;

    bool doLogout() const;

    bool doSair() const;

    bool doConsultarClientes() const;

    bool doCadastrarClientes() const;

    bool doDevolucaoVeiculo() const;

    bool doAlugarVeiculo() const;

    // ----------------------------------------------------------------------
    // - ATRIBUTOS                                                          -
    // ----------------------------------------------------------------------
    MenuControl *_control;

    AluguelView *_aluguel_view;

};


#endif //PA_TRABALHO_2017_LOGINMENU_H
